const swiper2 = new Swiper('.swiper2', {

    // Optional parameters
    direction: 'vertical',
    spaceBetween: 100,
    loop: true,
    slidesPerView: 'auto',

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },
    // Navigation arrows

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});