/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';


//mapbox
        
// import mapboxgl from 'mapbox-gl'; // or "const mapboxgl = require('mapbox-gl');"
// mapboxgl.accessToken = 'pk.eyJ1IjoiZGF2aWRkZDEiLCJhIjoiY2t3cTU2eWx3MDNnbzJubzl3MHRwMmQydCJ9.cXqaTDKUiN0mK0obF0_IWw';
// const map = new mapboxgl.Map({
//     container: 'map', // container ID
//     style: 'mapbox://styles/daviddd1/cl0wp05fg001y14o1pxs9qop9', // style URL
//     center: [-74.5, 40], // starting position [lng, lat]
//     zoom: 9 // starting zoom
// });


// function marker(lng,lat){
// const marker = new mapboxgl.Marker()
// .setLngLat([lng,lat])
// .addTo(map)
// return marker
// }



// import 'mapbox-gl/dist/mapbox-gl.css';


