<?php

namespace App\Form;

use App\Entity\UsersProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use phpDocumentor\Reflection\File;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;



class UsersProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('biography')


            ->add('avatar', HiddenType::class, [
                'attr' => [
                    'placeholder' =>    "avatar"
                ]
            ])

            ->add('avatarFile', VichFileType::class, [
                'label'=> "ajouter un avatar",
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])






            ->add('url')


        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UsersProfile::class,
        ]);
    }
}
