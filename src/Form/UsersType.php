<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('roles',  ChoiceType::class, [
                'label' => "Roles",
                'choice' => [
                    'Membre'=> "ROLE_USER",
                    'ADMIN'=> "ROLE_ADMIN"
                ],
                'multiple' => true,
                'expanded'=> true
                ] )

            ->add('lastname')
            ->add('firstname')
            ->add('pseudo')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
