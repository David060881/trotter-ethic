<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Article;
use App\Entity\Countries;
use App\Entity\Status;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use phpDocumentor\Reflection\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('baseline', CKEditorType::class, [
                "config_name" => 'full'
            ] )


            ->add('HeroImage', HiddenType::class, [
                'attr' => [
                    'placeholder' =>    "Rentrer l'url de l'image a mettre dans le bandeau"
                    ]
            ])

            ->add('HeroImageFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])


            ->add('contentImage',HiddenType::class, [
                'attr' => [
                    'label' =>  "Rentrer l'url de l'image du contenu principal",
                    'placeholder' =>    "Rentrer l'url de l'image du contenu principal"
                ]
            ])
            ->add('contentImageFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])

            ->add('content2Image',HiddenType::class, [
                'attr' => [
                    'label' =>  "Rentrer l'url de l'image du contenu secondaire",
                    'placeholder' =>    "Rentrer l'url de l'image du contenu secondaire"
                ]
            ])
            ->add('content2ImageFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])

            ->add('imageIntersection',HiddenType::class, [
                'attr' => [
                    'label' =>  "Rentrer l'url de l'image de séparation",
                    'placeholder' =>    "Rentrer l'url de l'image de séparation"
                ]
            ])

            ->add('imageIntersectionFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])


            ->add('thumbnail', HiddenType::class, [
                   'attr' => [
                       'placeholder' => "Rentrer l'url de l'image a la une"
                   ]
                ])

            ->add('imageFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])




            ->add('excerpt', CKEditorType::class, [
               "config_name" => 'full'
                ] )

            ->add('content', CKEditorType::class, [
                "config_name" => 'full'
            ] )
            ->add('content2', CKEditorType::class, [
                "config_name" => 'full'
            ] )
            ->add('categories')
            ->add('countries', EntityType::class,[
                'class' =>Countries::class,
                'choice_label' => 'name'
            ])
            ->add('images', FileType::class,[
                'label' => false,
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                ])


            ->add('status', EntityType::class,[
                'class' =>Status::class,
                'choice_label' => 'label'
            ]);


    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
