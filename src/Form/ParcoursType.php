<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Parcours;
use App\Entity\ParcoursCategories;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ParcoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('latitude')
            ->add('longitude')

            ->add('thumbnail', HiddenType::class, [
                'attr' => [
                    'placeholder' => "Rentrer l'url de l'image a la une"
                ]
            ])
            ->add('thumbnailFile', VichFileType::class, [
                'label' => "Rentrer l'url de l'image a la une",
                'required'=>false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
            ])

            ->add('content')
            ->add('article', EntityType::class, [
                'class' => Article::class,
                'choice_label' => 'title'
            ])
            ->add('parcoursCategories', EntityType::class,[
                'class' => ParcoursCategories::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Parcours::class,
        ]);
    }
}
