<?php

namespace App\Form;

use App\Entity\IntersetPoint;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IntersetPointType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('description', CKEditorType::class, [
                "config_name" => 'full'
            ] )
            ->add('latitude')
            ->add('longitude')
            ->add('categorieInterest')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IntersetPoint::class,
        ]);
    }
}
