<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\CarnetVoyage;
use App\Entity\Countries;
use App\Entity\Parcours;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CarnetVoyage2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                "label" => 'Titre'

            ])
            ->add('content', CKEditorType::class, [
                "config_name" => 'full',
                "label" => 'Contenu'
            ] )

            ->add('thumbnail', HiddenType::class)
            ->add('imageFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'asset_helper'=> true,
                "label" => 'Image a la une'
            ])


            ->add('imageContent',HiddenType::class)
            ->add('imageContentFile', VichFileType::class, [
                'required'=>false,
                'allow_delete' => true,
                'delete_label'=> '...',
                'download_label' => '...',
                'download_uri' => '...',
                'asset_helper'=> true,
                "label" => 'Image du contenu'
            ])

            ->add('country', EntityType::class,[
                'class' =>Countries::class,
                'choice_label' => 'name',
                "label" => 'Pays'

            ])
            ->add('parcour', EntityType::class, [
                'class' => Parcours::class,
                'choice_label' => 'name',
                'required' => true
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CarnetVoyage::class,
        ]);
    }
}
