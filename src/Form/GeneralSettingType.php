<?php

namespace App\Form;

use App\Entity\GeneralSetting;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class GeneralSettingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('WebsiteTitle', Null, [
                'label' => 'Titre du site'
            ])
            ->add('Logo',HiddenType::class, [
                'empty_data' => ''
            ])

            ->add('LogoFile',VichFileType::class,[
                'label'=> 'ajouter le logo du site',
                'allow_delete' => false,
                'asset_helper'=> true,
                'required' => true,

            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Ajouter une description au site'
            ])

            ->add('slogan', CKEditorType::class, [
                'label'=> 'Ajouter un slogan au site '])

            ->add('articlesContent', CKEditorType::class, [
                'label'=> 'Ajouter le texte visible sur la page "ARTICLES" '])

            ->add('carnetsContent', CKEditorType::class, [
                'label'=> 'Ajouter le texte visible sur la page "Carnet de voyage" '])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GeneralSetting::class,
        ]);
    }
}
