<?php

namespace App\Form;

use App\Entity\CarnetVoyage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarnetVoyage1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('Content')
            ->add('thumbnail')
            ->add('imageContent')
            ->add('createdAt')
            ->add('updateAt')
            ->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CarnetVoyage::class,
        ]);
    }
}
