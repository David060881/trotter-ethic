<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Repository\GeneralSettingRepository;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TwigEventsSubscriber implements EventSubscriberInterface
{


    private  $twig;
    private  $generalSettingRepository;

    public function __construct(Environment $twig, GeneralSettingRepository $generalSettingRepository)
    {
        $this->twig = $twig;
        $this->generalSettingRepository = $generalSettingRepository;

    }
    public function onControllerEvent(ControllerEvent $event): void
    {
        // ...
        $this->twig->addGlobal('generalSettings', $this->generalSettingRepository->findOneBy([],['id'=> 'ASC']));

    }

    public static function getSubscribedEvents(): array
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
