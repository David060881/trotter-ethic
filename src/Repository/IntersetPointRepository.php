<?php

namespace App\Repository;

use App\Entity\IntersetPoint;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method IntersetPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method IntersetPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method IntersetPoint[]    findAll()
 * @method IntersetPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntersetPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IntersetPoint::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(IntersetPoint $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(IntersetPoint $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    /*
    terminer la query est vérifié son fonctionnement
    */

    function findByCategoriesInterest($value, $limit = 30) {
        $entityManager = $this->getEntityManager();
        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata("\App\Entity\IntersetPoint", 'IntersetPoint');
        
        $query = $entityManager->createNativeQuery(
            "
                SELECT interset_point.* 
                FROM interset_point
                INNER JOIN categories_interest 
                ON interset_point.categorie_interest_id = categories_interest.id
                WHERE categories_interest.title = ?
                limit ?
            ", $rsm
        )->setParameters([1 => $value, 2 => $limit]);

        return $query->getResult();

    }






    // /**
    //  * @return IntersetPoint[] Returns an array of IntersetPoint objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IntersetPoint
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
