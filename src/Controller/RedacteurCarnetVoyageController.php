<?php

namespace App\Controller;

use App\Entity\CarnetVoyage;
use App\Form\CarnetVoyage2Type;
use App\Repository\CarnetVoyageRepository;
use http\Client\Curl\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/redacteur/carnet/voyage')]
class RedacteurCarnetVoyageController extends AbstractController
{
    #[Route('/', name: 'app_redacteur_carnet_voyage_index', methods: ['GET'])]
    public function index(CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        return $this->render('redacteur_carnet_voyage/index.html.twig', [
            'carnet_voyages' => $carnetVoyageRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_redacteur_carnet_voyage_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        $carnetVoyage = new CarnetVoyage();
        $user = $this->getUser();

        $form = $this->createForm(CarnetVoyage2Type::class, $carnetVoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carnetVoyage->setUsers($user); // faux positif
            $carnetVoyage->setCreatedAt(new \DateTimeImmutable());
            $carnetVoyage->setUpdateAt(new \DateTimeImmutable());
            $carnetVoyageRepository->add($carnetVoyage);
            return $this->redirectToRoute('app_redacteur_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('redacteur_carnet_voyage/new.html.twig', [
            'carnet_voyage' => $carnetVoyage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_redacteur_carnet_voyage_show', methods: ['GET'])]
    public function show(CarnetVoyage $carnetVoyage): Response
    {
        return $this->render('redacteur_carnet_voyage/show.html.twig', [
            'carnet_voyage' => $carnetVoyage,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_redacteur_carnet_voyage_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CarnetVoyage $carnetVoyage, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        $form = $this->createForm(CarnetVoyage2Type::class, $carnetVoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carnetVoyageRepository->add($carnetVoyage);
            return $this->redirectToRoute('app_redacteur_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('redacteur_carnet_voyage/edit.html.twig', [
            'carnet_voyage' => $carnetVoyage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_redacteur_carnet_voyage_delete', methods: ['POST'])]
    public function delete(Request $request, CarnetVoyage $carnetVoyage, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carnetVoyage->getId(), $request->request->get('_token'))) {
            $carnetVoyageRepository->remove($carnetVoyage);
        }

        return $this->redirectToRoute('app_redacteur_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
    }
}
