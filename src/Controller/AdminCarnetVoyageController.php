<?php

namespace App\Controller;

use App\Entity\CarnetVoyage;
use App\Form\CarnetVoyage1Type;
use App\Repository\CarnetVoyageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/carnet/voyage')]
class AdminCarnetVoyageController extends AbstractController
{
    #[Route('/', name: 'app_admin_carnet_voyage_index', methods: ['GET'])]
    public function index(CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        return $this->render('admin_carnet_voyage/index.html.twig', [
            'carnet_voyages' => $carnetVoyageRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_carnet_voyage_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        $carnetVoyage = new CarnetVoyage();
        $form = $this->createForm(CarnetVoyage1Type::class, $carnetVoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carnetVoyageRepository->add($carnetVoyage);
            return $this->redirectToRoute('app_admin_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_carnet_voyage/new.html.twig', [
            'carnet_voyage' => $carnetVoyage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_carnet_voyage_show', methods: ['GET'])]
    public function show(CarnetVoyage $carnetVoyage): Response
    {
        return $this->render('admin_carnet_voyage/show.html.twig', [
            'carnet_voyage' => $carnetVoyage,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_carnet_voyage_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CarnetVoyage $carnetVoyage, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        $form = $this->createForm(CarnetVoyage1Type::class, $carnetVoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carnetVoyageRepository->add($carnetVoyage);
            return $this->redirectToRoute('app_admin_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_carnet_voyage/edit.html.twig', [
            'carnet_voyage' => $carnetVoyage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_carnet_voyage_delete', methods: ['POST'])]
    public function delete(Request $request, CarnetVoyage $carnetVoyage, CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carnetVoyage->getId(), $request->request->get('_token'))) {
            $carnetVoyageRepository->remove($carnetVoyage);
        }

        return $this->redirectToRoute('app_admin_carnet_voyage_index', [], Response::HTTP_SEE_OTHER);
    }
}
