<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CategoriesInterestRepository;
use App\Repository\IntersetPointRepository;
use App\Repository\ParcoursRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(IntersetPointRepository $intersetPointRepository, CategoriesInterestRepository $categoriesInterestRepository, ArticleRepository $articleRepository, ParcoursRepository $parcoursRepository): Response
    {
        //récuperation de toutes les catégories de point d'interet 
        $catergoriesPoi = $categoriesInterestRepository->findAll();
        
        //récuperation de toutes les points d'interet 
        $POI = $intersetPointRepository->findAll();
        //récuperation de tous les articles
        $articles = $articleRepository->findAll();

        $parcours = $parcoursRepository->findAll();

    
        $filterCategs =  $intersetPointRepository->findByCategoriesInterest('Restaurant', 20); 
        
        return $this->render('main/index.html.twig', [
            'PointsInterests' => $POI,
            'POIcategories' => $catergoriesPoi,
            'PointsInterestsBycategorie' => $filterCategs,
            'articles' => $articles,
            'parcours' => $parcours
        ]);
        
        
    }


    #[Route('map/categorie', methods: ['GET'], name: 'map_categorie')]
    public function mapCateg( Request $request, IntersetPointRepository $intersetPointRepository) : JsonResponse
    {
        $cat = $request->query->get('cat');
        $filterCategs =  $intersetPointRepository->findByCategoriesInterest($cat, 20); 
   
        $result = [];
        foreach ($filterCategs as $item) {
            $result[] = [
                'id' => $item->getId(),
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'latitude' => $item->getLatitude(),
                'longitude' => $item->getLongitude(),
                'categorie' => $cat

            ];
        }
        return new JsonResponse($result);

    }
    



}
