<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Article;
use App\Entity\Categories;
use App\Entity\Images;
use App\Form\ArticleType;
use App\Repository\ParcoursRepository;
use PhpParser\Node\Expr\New_;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{
    #[Route('/article', name: 'app_article')]
    public function index(ArticleRepository $articleRepo ): Response
    {
        $articles  = $articleRepo ->findAll();

        return $this->render('article/articles.html.twig', [
           'articles'  => $articles,
            'controller_name' => "Les articles"
    ]);

    }
    #[Route('/article/new', name:'article_new')]
    
    public function article_new(Request $request, EntityManagerInterface $manager): Response {
        $article = new Article();
        $form =  $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $images = $form->get('images') -> getData();
            foreach($images as $image)
            {
               $files = md5(uniqid()) . '.' . $image->gessExtension();
               $image->move(
                   $this->getParameter('image_directory'),
                   $files
               );
               $img =new Images();
               $img ->setName($files);
               $article->addImage($img);
            }

            $article->setCreatedAt(new \DateTime());
            $article->setUpdateAt(new \DateTime());
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }

    return $this->render('article/new.html.twig', [
        'formArticle' => $form->createView(),
        'pageTitle' => "Ajouter un article"
    ]);

      
    }


    #[Route("article/{slug}", name:'article_show_slug')]
    public function article_show_slug(Article $article, ParcoursRepository $parcoursRepository, $slug, ArticleRepository $articleRepository): Response
    {
        $parcours = $parcoursRepository->findAll();
        $articles = $articleRepository->findAll();

        
        return $this->render('article/show.html.twig', [ 
            'article'=> $article,
            'articles' => $articles,
            'parcours' => $parcours,
        ]);
    }

    #[Route("article/{id}/show", name:'article_show')]
    public function article_show(Article $article, ParcoursRepository $parcoursRepository,ArticleRepository $articleRepository): Response
    {
        $parcours = $parcoursRepository->findAll();
        $articles = $articleRepository->findAll();

        return $this->render('article/show.html.twig', [
            'article'=> $article,
            'articles' => $articles,
            'parcours' => $parcours,
        ]);
    }
}
