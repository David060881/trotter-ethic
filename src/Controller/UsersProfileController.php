<?php

namespace App\Controller;

use App\Entity\UsersProfile;
use App\Form\UsersProfileType;
use App\Repository\ArticleRepository;
use App\Repository\UsersProfileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



#[Route('/users/profile')]
class UsersProfileController extends AbstractController
{


    #[Route('/{id}', name: 'app_users_profile_show', methods: ['GET'])]
    public function show(UsersProfile $usersProfile, ArticleRepository $articleRepository ): Response
    {

        if($this->getUser() == $usersProfile->getUser()){

            $canEdit = true;
        }

        return $this->render('users_profile/show.html.twig', [
            'users_profile' => $usersProfile,
            'can_edit' => $canEdit
        ]);
    }

    #[Route('/{id}/edit', name: 'app_users_profile_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UsersProfile $usersProfile, UsersProfileRepository $usersProfileRepository): Response
    {
            //controle que l'utlitisateur actuel est le propriétaire du profil
        if ($this->getUser() != $usersProfile->getUser()) { 
            $this->addFlash('warning', "Opération interdite");
            
            //redirection sur la page d'accueil 
            return $this->redirectToRoute('app_users_profile_show');
        }

        $form = $this->createForm(UsersProfileType::class, $usersProfile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usersProfileRepository->add($usersProfile);
            return $this->redirectToRoute('main', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('users_profile/edit.html.twig', [
            'users_profile' => $usersProfile,
            'form' => $form,
        ]);
    }

}
