<?php

namespace App\Controller;

use App\Entity\GeneralSetting;
use App\Form\GeneralSettingEditType;
use App\Form\GeneralSettingType;
use App\Repository\GeneralSettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/general/setting')]
class AdminGeneralSettingController extends AbstractController
{
    #[Route('/', name: 'app_admin_general_setting_index', methods: ['GET'])]
    public function index(GeneralSettingRepository $generalSettingRepository): Response
    {
        return $this->render('admin_general_setting/index.html.twig', [
            'general_settings' => $generalSettingRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_general_setting_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GeneralSettingRepository $generalSettingRepository): Response
    {
        $generalSetting = new GeneralSetting();
        $form = $this->createForm(GeneralSettingType::class, $generalSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $generalSetting->setCreatedAt(new \DateTimeImmutable());
            $generalSetting->setUpdateAt(new \DateTimeImmutable());
            $generalSettingRepository->add($generalSetting);
            return $this->redirectToRoute('app_admin_general_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_general_setting/new.html.twig', [
            'general_setting' => $generalSetting,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_general_setting_show', methods: ['GET'])]
    public function show(GeneralSetting $generalSetting): Response
    {
        return $this->render('admin_general_setting/show.html.twig', [
            'general_setting' => $generalSetting,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_general_setting_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, GeneralSetting $generalSetting, GeneralSettingRepository $generalSettingRepository): Response
    {
        $form = $this->createForm(GeneralSettingEditType::class, $generalSetting);
        $form->handleRequest($request);
       // dd($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $generalSetting->setUpdateAt(new \DateTimeImmutable());
            $generalSettingRepository->add($generalSetting);
            return $this->redirectToRoute('app_admin_general_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_general_setting/edit.html.twig', [
            'general_setting' => $generalSetting,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_general_setting_delete', methods: ['POST'])]
    public function delete(Request $request, GeneralSetting $generalSetting, GeneralSettingRepository $generalSettingRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$generalSetting->getId(), $request->request->get('_token'))) {
            $generalSettingRepository->remove($generalSetting);
        }

        return $this->redirectToRoute('app_admin_general_setting_index', [], Response::HTTP_SEE_OTHER);
    }
}
