<?php

namespace App\Controller;

use App\Entity\CarnetVoyage;
use App\Repository\CarnetVoyageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarnetVoyageController extends AbstractController
{


    #[Route('/carnet/voyage', name: 'app_carnet_voyage')]
    public function index(CarnetVoyageRepository $carnetVoyageRepository): Response
    {
        $carnetsVoyage = $carnetVoyageRepository->findAll();
        return $this->render('carnet_voyage/index.html.twig', [
            'controller_name' => 'Les carnets de voyage',
            'carnet_voyages' => $carnetsVoyage
        ]);
    }

    #[Route("carnet/{id}/show", name:'carnet_show')]
    public function article_show(CarnetVoyage $carnetVoyage  ): Response
    {

        return $this->render('carnet_voyage/show.html.twig', [
            'carnet'=> $carnetVoyage,

        ]);
    }

    #[Route("carnet/{slug}", name:'carnet_slug')]
    public function article_show_slug(CarnetVoyage $carnetVoyage, $slug): Response
    {

        return $this->render('carnet_voyage/show.html.twig', [
            'carnet'=> $carnetVoyage,
        ]);
    }
}
