<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Form\Activity1Type;
use App\Repository\ActivityRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/activity')]
class AdminActivityController extends AbstractController
{
    #[Route('/', name: 'app_admin_activity_index', methods: ['GET'])]
    public function index(ActivityRepository $activityRepository,ArticleRepository $articleRepository): Response
    {
        $activities = $activityRepository->findAll();



        return $this->render('admin_activity/index.html.twig', [
            'activities' => $activities,
        ]);
    }

    #[Route('/new', name: 'app_admin_activity_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActivityRepository $activityRepository): Response
    {
        $activity = new Activity();
        $form = $this->createForm(Activity1Type::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityRepository->add($activity);
            return $this->redirectToRoute('app_admin_activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_activity/new.html.twig', [
            'activity' => $activity,
            'form' => $form,
        ]);
    }
    #[Route('/new/{id}', name: 'app_admin_activity_new_by_id', methods: ['GET', 'POST'])]
    public function newById(Request $request, ActivityRepository $activityRepository,$id, ArticleRepository $articleRepository): Response
    {
        $activity = new Activity();
        $form = $this->createForm(Activity1Type::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activity->setArticle($articleRepository->findOneBy(['id'=>$id]));
            $activityRepository->add($activity);

            if(isset($_POST['END'])){
            return $this->redirectToRoute('app_admin_activity_index', [], Response::HTTP_SEE_OTHER);
            } else {
                return $this->redirectToRoute('app_admin_activity_new_by_id', ['id'=>$id], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('admin_activity/new.html.twig', [
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_activity_show', methods: ['GET'])]
    public function show(Activity $activity): Response
    {
        return $this->render('admin_activity/show.html.twig', [
            'activity' => $activity,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_activity_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Activity $activity, ActivityRepository $activityRepository): Response
    {
        $form = $this->createForm(Activity1Type::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityRepository->add($activity);
            return $this->redirectToRoute('app_admin_activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_activity/edit.html.twig', [
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_activity_delete', methods: ['POST'])]
    public function delete(Request $request, Activity $activity, ActivityRepository $activityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$activity->getId(), $request->request->get('_token'))) {
            $activityRepository->remove($activity);
        }

        return $this->redirectToRoute('app_admin_activity_index', [], Response::HTTP_SEE_OTHER);
    }
}
