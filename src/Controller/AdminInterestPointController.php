<?php

namespace App\Controller;

use App\Entity\IntersetPoint;
use App\Form\IntersetPointType;
use App\Repository\IntersetPointRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/interest/point')]
class AdminInterestPointController extends AbstractController
{
    #[Route('/', name: 'app_admin_interest_point_index', methods: ['GET'])]
    public function index(IntersetPointRepository $intersetPointRepository): Response
    {
        return $this->render('admin_interest_point/index.html.twig', [
            'interset_points' => $intersetPointRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_interest_point_new', methods: ['GET', 'POST'])]
    public function new(Request $request, IntersetPointRepository $intersetPointRepository): Response
    {
        $intersetPoint = new IntersetPoint();
        $form = $this->createForm(IntersetPointType::class, $intersetPoint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $intersetPointRepository->add($intersetPoint);
            return $this->redirectToRoute('app_admin_interest_point_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_interest_point/new.html.twig', [
            'interset_point' => $intersetPoint,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_interest_point_show', methods: ['GET'])]
    public function show(IntersetPoint $intersetPoint): Response
    {
        return $this->render('admin_interest_point/show.html.twig', [
            'interset_point' => $intersetPoint,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_interest_point_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, IntersetPoint $intersetPoint, IntersetPointRepository $intersetPointRepository): Response
    {
        $form = $this->createForm(IntersetPointType::class, $intersetPoint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $intersetPointRepository->add($intersetPoint);
            return $this->redirectToRoute('app_admin_interest_point_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_interest_point/edit.html.twig', [
            'interset_point' => $intersetPoint,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_interest_point_delete', methods: ['POST'])]
    public function delete(Request $request, IntersetPoint $intersetPoint, IntersetPointRepository $intersetPointRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$intersetPoint->getId(), $request->request->get('_token'))) {
            $intersetPointRepository->remove($intersetPoint);
        }

        return $this->redirectToRoute('app_admin_interest_point_index', [], Response::HTTP_SEE_OTHER);
    }
}
