<?php

namespace App\Controller;

use App\Entity\Countries;
use App\Form\CountriesType;
use App\Repository\CountriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/countries/controlller')]
class AdminCountriesControlllerController extends AbstractController
{
    #[Route('/', name: 'app_admin_countries_controlller_index', methods: ['GET'])]
    public function index(CountriesRepository $countriesRepository): Response
    {
        return $this->render('admin_countries_controlller/index.html.twig', [
            'countries' => $countriesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_countries_controlller_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CountriesRepository $countriesRepository): Response
    {
        $country = new Countries();
        $form = $this->createForm(CountriesType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $countriesRepository->add($country);
            return $this->redirectToRoute('app_admin_countries_controlller_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_countries_controlller/new.html.twig', [
            'country' => $country,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_countries_controlller_show', methods: ['GET'])]
    public function show(Countries $country): Response
    {
        return $this->render('admin_countries_controlller/show.html.twig', [
            'country' => $country,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_countries_controlller_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Countries $country, CountriesRepository $countriesRepository): Response
    {
        $form = $this->createForm(CountriesType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $countriesRepository->add($country);
            return $this->redirectToRoute('app_admin_countries_controlller_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_countries_controlller/edit.html.twig', [
            'country' => $country,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_countries_controlller_delete', methods: ['POST'])]
    public function delete(Request $request, Countries $country, CountriesRepository $countriesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$country->getId(), $request->request->get('_token'))) {
            $countriesRepository->remove($country);
        }

        return $this->redirectToRoute('app_admin_countries_controlller_index', [], Response::HTTP_SEE_OTHER);
    }
}
