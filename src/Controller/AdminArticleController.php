<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Images;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CountriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/admin/article')]
class AdminArticleController extends AbstractController
{
    #[Route('/', name: 'app_admin_article_index', methods: ['GET'])]
    public function index(ArticleRepository $articleRepository, CountriesRepository $countriesRepository,ChartBuilderInterface $chartBuilder): Response
    {

       $articles = $articleRepository->findAll();

       $listCountry = $countriesRepository->findAll();
       $AllCountry = [];
       $articleByCountry = [];
       $colorCountry = [];


       foreach ($listCountry as $key =>$country){
        $AllCountry[] = [$country->getName()];
        }

       foreach ($AllCountry as $key => $countryName){

           $articleByCountry[$countryName[0]]= count($articleRepository->findByCountryDQL($countryName));
           $colorCountry[] = 'rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')';

       }

        $chartCountry = $chartBuilder->createChart(Chart::TYPE_DOUGHNUT);

        $chartCountry->setData(
            [
                'labels' => array_keys($articleByCountry),
                'datasets' => [
                    [
                        'label' => 'Nb d\'article par pays ',
                        'backgroundColor' => $colorCountry,
                        'borderColor' => $colorCountry,
                        'data' => array_values($articleByCountry) ,
                    ]

                ],
            ],
        );



        return $this->render('admin_article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
            'active' => 'nav_article',
            'chartcountry' => $chartCountry,
        ]);
    }

    #[Route('/new', name: 'app_admin_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $article = new Article();
        $user = $this->getUser();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuteur($user); // faux positif
            $article->setCreatedAt(new \DateTime());
            $article->setUpdateAt(new \DateTime());
            $entityManager->persist($article);
            $entityManager->flush();

            if(isset($_POST['END'])) {
                return $this->redirectToRoute('app_admin_article_index', [], Response::HTTP_SEE_OTHER);
            } else {
            return $this->redirectToRoute('app_admin_activity_new_by_id', ['id'=>$article->getId()], Response::HTTP_SEE_OTHER);
            }

        }

        return $this->renderForm('admin_article/new.html.twig', [
            'article' => $article,
            'form' => $form,
            'active' => 'nav_article',
        ]);
    }

    #[Route('/{id}', name: 'app_admin_article_show', methods: ['GET'])]
    public function show(Article $article): Response
    {
        return $this->render('admin_article/show.html.twig', [
            'article' => $article,
            'active' => 'nav_article',

        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Article $article, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->get('images')->getData();
            foreach ($images as $image) {
                $files = md5(uniqid()) . '.' . $image->guessExtension();
                $image->move(
                    $this->getParameter('image_directory'),
                    $files
                );
                $img = new Images();
                $img->setName($files);
                $article->addImage($img);
            }
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_activity_new', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
            'active' => 'nav_article'
        ]);
    }

    #[Route('/{id}', name: 'app_admin_article_delete', methods: ['POST'])]
    public function delete(Request $request, Article $article, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token'))) {
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_article_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/delete/image/{id}', name: 'article_delete_image', methods: ['DELETE'])]
    public function deleteImage(Images $image, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid('delete'.$image->getId(), $data['_token'])) {
        $name = $image->getName();
            unlink($this->getParameter(('image_directory').'/'.$name));
            $entityManager->remove($image);
            $entityManager->flush();
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
}
