<?php

namespace App\Controller;

use App\Entity\CustomPage;
use App\Repository\CustomPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomPageController extends AbstractController
{


    #[Route('page/{url}', name: 'custom_page_a_propos', methods: ['GET'])]
    public function showApropos(CustomPage $customPage, CustomPageRepository $customPageRepository ,$url): Response
    {
        $page = $customPageRepository->findOneBy(['url' => $url]);
        return $this->render('custom_page/apropos.html.twig', [
            'page' => $page
        ]);
    }


}
