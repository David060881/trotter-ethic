<?php

namespace App\Controller;

use App\Entity\Users;
use App\Entity\UsersProfile;
use App\Repository\ArticleRepository;
use App\Repository\UsersRepository;
use Doctrine\Migrations\Tools\Console\Command\UpToDateCommand;
use Symfony\UX\Chartjs\Model\Chart;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(ChartBuilderInterface $chartBuilder, ArticleRepository $articleRepository, UsersRepository $usersRepository): Response
    {


        $auteurs = $usersRepository->findAll();
        $nbPostsByAuthors = [];

        foreach ($auteurs as $key => $auteur) {
            $nbPostsByAuthors[$auteur->getPseudo()] = count($auteur->getArticles());

        }

        //  dd($nbPostsByAuthors);


        $articles2020 = $this->getArticleByYears(2020, $auteurs, $articleRepository);
        $articles2021 = $this->getArticleByYears(2021, $auteurs, $articleRepository);
        $articles2022 = $this->getArticleByYears(2022, $auteurs, $articleRepository);

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $chart2 = $chartBuilder->createChart(Chart::TYPE_BAR);

        $chart2->setData(
            [
                'labels' => array_keys($nbPostsByAuthors),
                'datasets' => [
                    [
                        'label' => 'Nb de poste /auteur 2022',
                        'backgroundColor' => 'rgb(255, 0, 132)',
                        'borderColor' => 'rgb(255, 0, 132)',
                        'data' => array_values($articles2022),
                    ],
                ],
            ],
        );
        $chart2->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 10,
                ],
            ],
        ]);

        $chart->setData(
            [
                'labels' => array_keys($nbPostsByAuthors),
                'datasets' => [
                    [
                        'label' => 'Nb de poste total par auteur ',
                        'backgroundColor' => 'rgb(255, 99, 132)',
                        'borderColor' => 'rgb(255, 99, 132)',
                        'data' => array_values($nbPostsByAuthors),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2020',
                        'backgroundColor' => 'rgb(255, 99, 0)',
                        'borderColor' => 'rgb(255, 99, 0)',
                        'data' => array_values($articles2020),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2021',
                        'backgroundColor' => 'rgb(0, 99, 132)',
                        'borderColor' => 'rgb(0, 99, 132)',
                        'data' => array_values($articles2021),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2022',
                        'backgroundColor' => 'rgb(255, 0, 132)',
                        'borderColor' => 'rgb(255, 0, 132)',
                        'data' => array_values($articles2022),
                    ],
                ],
            ],
        );
        $chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 10,
                ],
            ],
        ]);



        return $this->render('admin/index.html.twig', [
            'chart' => $chart,
            'UsersList' => $auteurs,
            'chart2'=> $chart2,
        ]);
    }


    function getArticleByYears($year, $auteurs, ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findByYear($year);
        $nbPostsByAuthors = [];
        foreach ($auteurs as $key => $auteur)
        {
            $nbPostsByAuthors[$auteur->getPseudo()] = 0;
        }
        foreach ($articles as $key => $article)
        {
            $nbPostsByAuthors[$article->getAuteur()->getPseudo()]++;
        }
        return $nbPostsByAuthors;
    }
}
