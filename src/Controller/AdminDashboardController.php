<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\UsersRepository;
use Doctrine\Migrations\Tools\Console\Command\UpToDateCommand;
use Symfony\UX\Chartjs\Model\Chart;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]

    public function index(ChartBuilderInterface $chartBuilder, ArticleRepository $articleRepository, UsersRepository $usersRepository): Response
    {

        $auteurs = $usersRepository->findAll();


        return $this->render('admin_dashboard/index.html.twig', [
            'auteurs' => $auteurs
        ]);
}




}
