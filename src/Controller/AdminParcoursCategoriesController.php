<?php

namespace App\Controller;

use App\Entity\ParcoursCategories;
use App\Form\ParcoursCategoriesType;
use App\Repository\ParcoursCategoriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/parcours/categories')]
class AdminParcoursCategoriesController extends AbstractController
{
    #[Route('/', name: 'app_admin_parcours_categories_index', methods: ['GET'])]
    public function index(ParcoursCategoriesRepository $parcoursCategoriesRepository): Response
    {
        return $this->render('admin_parcours_categories/index.html.twig', [
            'parcours_categories' => $parcoursCategoriesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_parcours_categories_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ParcoursCategoriesRepository $parcoursCategoriesRepository): Response
    {
        $parcoursCategory = new ParcoursCategories();
        $form = $this->createForm(ParcoursCategoriesType::class, $parcoursCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parcoursCategoriesRepository->add($parcoursCategory);
            return $this->redirectToRoute('app_admin_parcours_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_parcours_categories/new.html.twig', [
            'parcours_category' => $parcoursCategory,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_parcours_categories_show', methods: ['GET'])]
    public function show(ParcoursCategories $parcoursCategory): Response
    {
        return $this->render('admin_parcours_categories/show.html.twig', [
            'parcours_category' => $parcoursCategory,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_parcours_categories_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ParcoursCategories $parcoursCategory, ParcoursCategoriesRepository $parcoursCategoriesRepository): Response
    {
        $form = $this->createForm(ParcoursCategoriesType::class, $parcoursCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parcoursCategoriesRepository->add($parcoursCategory);
            return $this->redirectToRoute('app_admin_parcours_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_parcours_categories/edit.html.twig', [
            'parcours_category' => $parcoursCategory,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_parcours_categories_delete', methods: ['POST'])]
    public function delete(Request $request, ParcoursCategories $parcoursCategory, ParcoursCategoriesRepository $parcoursCategoriesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$parcoursCategory->getId(), $request->request->get('_token'))) {
            $parcoursCategoriesRepository->remove($parcoursCategory);
        }

        return $this->redirectToRoute('app_admin_parcours_categories_index', [], Response::HTTP_SEE_OTHER);
    }
}
