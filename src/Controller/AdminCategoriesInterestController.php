<?php

namespace App\Controller;

use App\Entity\CategoriesInterest;
use App\Form\CategoriesInterestType;
use App\Repository\CategoriesInterestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/categories/interest')]
class AdminCategoriesInterestController extends AbstractController
{
    #[Route('/', name: 'app_admin_categories_interest_index', methods: ['GET'])]
    public function index(CategoriesInterestRepository $categoriesInterestRepository): Response
    {
        return $this->render('admin_categories_interest/index.html.twig', [
            'categories_interests' => $categoriesInterestRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_categories_interest_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoriesInterestRepository $categoriesInterestRepository): Response
    {
        $categoriesInterest = new CategoriesInterest();
        $form = $this->createForm(CategoriesInterestType::class, $categoriesInterest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoriesInterestRepository->add($categoriesInterest);
            return $this->redirectToRoute('app_admin_categories_interest_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_categories_interest/new.html.twig', [
            'categories_interest' => $categoriesInterest,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_categories_interest_show', methods: ['GET'])]
    public function show(CategoriesInterest $categoriesInterest): Response
    {
        return $this->render('admin_categories_interest/show.html.twig', [
            'categories_interest' => $categoriesInterest,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_categories_interest_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategoriesInterest $categoriesInterest, CategoriesInterestRepository $categoriesInterestRepository): Response
    {
        $form = $this->createForm(CategoriesInterestType::class, $categoriesInterest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoriesInterestRepository->add($categoriesInterest);
            return $this->redirectToRoute('app_admin_categories_interest_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_categories_interest/edit.html.twig', [
            'categories_interest' => $categoriesInterest,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_categories_interest_delete', methods: ['POST'])]
    public function delete(Request $request, CategoriesInterest $categoriesInterest, CategoriesInterestRepository $categoriesInterestRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoriesInterest->getId(), $request->request->get('_token'))) {
            $categoriesInterestRepository->remove($categoriesInterest);
        }

        return $this->redirectToRoute('app_admin_categories_interest_index', [], Response::HTTP_SEE_OTHER);
    }
}
