<?php

namespace App\Controller;

use App\Entity\ActivityCategories;
use App\Form\ActivityCategoriesType;
use App\Repository\ActivityCategoriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/activity/categories')]
class AdminActivityCategoriesController extends AbstractController
{
    #[Route('/', name: 'app_admin_activity_categories_index', methods: ['GET'])]
    public function index(ActivityCategoriesRepository $activityCategoriesRepository): Response
    {
        return $this->render('admin_activity_categories/index.html.twig', [
            'activity_categories' => $activityCategoriesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_activity_categories_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActivityCategoriesRepository $activityCategoriesRepository): Response
    {
        $activityCategory = new ActivityCategories();
        $form = $this->createForm(ActivityCategoriesType::class, $activityCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityCategoriesRepository->add($activityCategory);
            return $this->redirectToRoute('app_admin_activity_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_activity_categories/new.html.twig', [
            'activity_category' => $activityCategory,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_activity_categories_show', methods: ['GET'])]
    public function show(ActivityCategories $activityCategory): Response
    {
        return $this->render('admin_activity_categories/show.html.twig', [
            'activity_category' => $activityCategory,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_activity_categories_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActivityCategories $activityCategory, ActivityCategoriesRepository $activityCategoriesRepository): Response
    {
        $form = $this->createForm(ActivityCategoriesType::class, $activityCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityCategoriesRepository->add($activityCategory);
            return $this->redirectToRoute('app_admin_activity_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_activity_categories/edit.html.twig', [
            'activity_category' => $activityCategory,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_activity_categories_delete', methods: ['POST'])]
    public function delete(Request $request, ActivityCategories $activityCategory, ActivityCategoriesRepository $activityCategoriesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$activityCategory->getId(), $request->request->get('_token'))) {
            $activityCategoriesRepository->remove($activityCategory);
        }

        return $this->redirectToRoute('app_admin_activity_categories_index', [], Response::HTTP_SEE_OTHER);
    }
}
