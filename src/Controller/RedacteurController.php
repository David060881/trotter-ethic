<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedacteurController extends AbstractController
{
    #[Route('/redacteur', name: 'app_redacteur')]
    public function index(): Response
    {
        return $this->render('redacteur/index.html.twig', [
            'controller_name' => 'RedacteurController',
        ]);
    }
}
