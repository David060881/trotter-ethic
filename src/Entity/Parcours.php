<?php

namespace App\Entity;

use App\Repository\ParcoursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ParcoursRepository::class)]
#[Vich\Uploadable]
class Parcours
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Vich\UploadableField(mapping: 'parcours_image', fileNameProperty: 'thumbnail')]
    private ?File $thumbnailFile = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $thumbnail;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $content;

    #[ORM\ManyToOne(targetEntity: Article::class, inversedBy: 'parcours')]
    private $article;

    #[ORM\ManyToOne(targetEntity: ParcoursCategories::class, inversedBy: 'parcours')]
    private $parcoursCategories;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $updateAt;

    #[ORM\OneToMany(mappedBy: 'parcour', targetEntity: CarnetVoyage::class)]
    private $carnetVoyages;

    #[ORM\Column(type: 'float', nullable: true)]
    private $latitude;

    #[ORM\Column(type: 'float')]
    private $longitude;

    public function __construct()
    {
        $this->carnetVoyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getParcoursCategories(): ?ParcoursCategories
    {
        return $this->parcoursCategories;
    }

    public function setParcoursCategories(?ParcoursCategories $parcoursCategories): self
    {
        $this->parcoursCategories = $parcoursCategories;

        return $this;
    }


    /**
     * @return File|null
     */
    public function getThumbnailFile(): ?File
    {
        return $this->thumbnailFile;
    }

    /**
     * @param File|null $thumbnailFile
     */
    public function setThumbnailFile(?File $thumbnailFile): void
    {
        if (null !== $thumbnailFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
        $this->thumbnailFile = $thumbnailFile;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection<int, CarnetVoyage>
     */
    public function getCarnetVoyages(): Collection
    {
        return $this->carnetVoyages;
    }

    public function addCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if (!$this->carnetVoyages->contains($carnetVoyage)) {
            $this->carnetVoyages[] = $carnetVoyage;
            $carnetVoyage->setParcour($this);
        }

        return $this;
    }

    public function removeCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if ($this->carnetVoyages->removeElement($carnetVoyage)) {
            // set the owning side to null (unless already changed)
            if ($carnetVoyage->getParcour() === $this) {
                $carnetVoyage->setParcour(null);
            }
        }

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }
}
