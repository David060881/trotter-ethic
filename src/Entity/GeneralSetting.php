<?php

namespace App\Entity;

use App\Repository\GeneralSettingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: GeneralSettingRepository::class)]
#[Vich\Uploadable]
class GeneralSetting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $WebsiteTitle;

    #[Vich\UploadableField(mapping: 'Logo_image', fileNameProperty: 'Logo')]
    private ?File $LogoFile = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $Logo = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $UpdateAt;

    #[ORM\Column(type: 'text', nullable: true)]
    private $articlesContent;

    #[ORM\Column(type: 'text', nullable: true)]
    private $CarnetsContent;

    #[ORM\Column(type: 'text', nullable: true)]
    private $slogan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWebsiteTitle(): ?string
    {
        return $this->WebsiteTitle;
    }

    public function setWebsiteTitle(string $WebsiteTitle): self
    {
        $this->WebsiteTitle = $WebsiteTitle;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->Logo;
    }

    /**
     * @return File|null
     */
    public function getLogoFile(): ?File
    {
        return $this->LogoFile;
    }

    /**
     * @param File|null $LogoFile
     */
    public function setLogoFile(?File $LogoFile = null): void
    {
        $this->LogoFile = $LogoFile;

        if (null !== $LogoFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->UpdateAt = new \DateTimeImmutable();
        }
    }


    public function setLogo(string $Logo): self
    {
        $this->Logo = $Logo;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->UpdateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $UpdateAt): self
    {
        $this->UpdateAt = $UpdateAt;

        return $this;
    }

    public function getArticlesContent(): ?string
    {
        return $this->articlesContent;
    }

    public function setArticlesContent(?string $articlesContent): self
    {
        $this->articlesContent = $articlesContent;

        return $this;
    }

    public function getCarnetsContent(): ?string
    {
        return $this->CarnetsContent;
    }

    public function setCarnetsContent(?string $CarnetsContent): self
    {
        $this->CarnetsContent = $CarnetsContent;

        return $this;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(?string $slogan): self
    {
        $this->slogan = $slogan;

        return $this;
    }
}
