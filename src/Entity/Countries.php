<?php

namespace App\Entity;

use App\Repository\CountriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountriesRepository::class)]
class Countries
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'countries', targetEntity: Article::class)]
    private $article;

    #[ORM\Column(type: 'string', length: 100)]
    private $name;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'float')]
    private $latitude;

    #[ORM\Column(type: 'float')]
    private $longitude;

    #[ORM\Column(type: 'string', length: 100)]
    private $climat;

    #[ORM\OneToMany(mappedBy: 'country', targetEntity: CarnetVoyage::class, orphanRemoval: true)]
    private $carnetVoyages;

    public function __construct()
    {
        $this->article = new ArrayCollection();
        $this->carnetVoyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticle(): Collection
    {
        return $this->article;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->article->contains($article)) {
            $this->article[] = $article;
            $article->setCountries($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->article->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getCountries() === $this) {
                $article->setCountries(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getClimat(): ?string
    {
        return $this->climat;
    }

    public function setClimat(string $climat): self
    {
        $this->climat = $climat;

        return $this;
    }

    /**
     * @return Collection<int, CarnetVoyage>
     */
    public function getCarnetVoyages(): Collection
    {
        return $this->carnetVoyages;
    }

    public function addCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if (!$this->carnetVoyages->contains($carnetVoyage)) {
            $this->carnetVoyages[] = $carnetVoyage;
            $carnetVoyage->setCountry($this);
        }

        return $this;
    }

    public function removeCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if ($this->carnetVoyages->removeElement($carnetVoyage)) {
            // set the owning side to null (unless already changed)
            if ($carnetVoyage->getCountry() === $this) {
                $carnetVoyage->setCountry(null);
            }
        }

        return $this;
    }
}
