<?php

namespace App\Entity;

use App\Repository\CategoriesInterestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoriesInterestRepository::class)]
class CategoriesInterest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\OneToMany(mappedBy: 'categorieInterest', targetEntity: IntersetPoint::class)]
    private $intersetPoints;
    

    public function __toString() {
        return $this->title;
    }

    public function __construct()
    {
        $this->intersetPoints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, IntersetPoint>
     */
    public function getIntersetPoints(): Collection
    {
        return $this->intersetPoints;
    }

    public function addIntersetPoint(IntersetPoint $intersetPoint): self
    {
        if (!$this->intersetPoints->contains($intersetPoint)) {
            $this->intersetPoints[] = $intersetPoint;
            $intersetPoint->setCategorieInterest($this);
        }

        return $this;
    }

    public function removeIntersetPoint(IntersetPoint $intersetPoint): self
    {
        if ($this->intersetPoints->removeElement($intersetPoint)) {
            // set the owning side to null (unless already changed)
            if ($intersetPoint->getCategorieInterest() === $this) {
                $intersetPoint->setCategorieInterest(null);
            }
        }

        return $this;
    }
}
