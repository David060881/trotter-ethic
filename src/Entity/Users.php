<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UsersRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class Users implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'array')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 100)]
    private $lastname;

    #[ORM\Column(type: 'string', length: 100)]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    private $pseudo;

    #[ORM\Column(type: 'datetime', options: ['default'=> 'CURRENT_TIMESTAMP'])]
    private $created_at;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: UsersProfile::class, cascade: ['persist', 'remove'])]
    private $usersProfile;

    #[ORM\OneToMany(mappedBy: 'auteur', targetEntity: Article::class)]
    private $articles;

    #[ORM\OneToMany(mappedBy: 'users', targetEntity: CarnetVoyage::class)]
    private $carnetVoyages;

    public function __construct()
    {
        $this->created_at =  new \DateTime();
        $this->articles = new ArrayCollection();
        $this->carnetVoyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string |null
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUsersProfile(): ?UsersProfile
    {
        return $this->usersProfile;
    }

    public function setUsersProfile(?UsersProfile $usersProfile): self
    {
        // unset the owning side of the relation if necessary
        if ($usersProfile === null && $this->usersProfile !== null) {
            $this->usersProfile->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($usersProfile !== null && $usersProfile->getUser() !== $this) {
            $usersProfile->setUser($this);
        }

        $this->usersProfile = $usersProfile;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setAuteur($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getAuteur() === $this) {
                $article->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CarnetVoyage>
     */
    public function getCarnetVoyages(): Collection
    {
        return $this->carnetVoyages;
    }

    public function addCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if (!$this->carnetVoyages->contains($carnetVoyage)) {
            $this->carnetVoyages[] = $carnetVoyage;
            $carnetVoyage->setUsers($this);
        }

        return $this;
    }

    public function removeCarnetVoyage(CarnetVoyage $carnetVoyage): self
    {
        if ($this->carnetVoyages->removeElement($carnetVoyage)) {
            // set the owning side to null (unless already changed)
            if ($carnetVoyage->getUsers() === $this) {
                $carnetVoyage->setUsers(null);
            }
        }

        return $this;
    }
}
