<?php

namespace App\Entity;

use phpDocumentor\Reflection\Types\String_;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;
use Gedmo\Mapping\Annotation as Gedmo;


#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[Vich\Uploadable]

class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $content;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'contentImage')]
    private ?File $contentImageFile = null;

    #[ORM\Column(type: 'string', length: 255)]
    private $contentImage;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'imageIntersection')]
    private ?File $imageIntersectionFile = null;



    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imageIntersection;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Images::class, cascade:['persist'])]
    private $images;

    #[ORM\ManyToOne(targetEntity: Categories::class, inversedBy: 'articles')]
    private $categories;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Comments::class)]
    private $comments;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\Column(type: 'datetime')]
    private $updateAt;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'thumbnail')]
    private ?File $imageFile = null;

    #[ORM\Column(type: 'string', length: 255)]
    private $thumbnail;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $HeroImage;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'HeroImage')]
    private ?File $HeroImageFile = null;

    #[ORM\ManyToOne(targetEntity: Users::class, inversedBy: 'articles')]
    private $auteur;

    #[ORM\ManyToOne(targetEntity: Countries::class, inversedBy: 'article')]
    private $countries;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Activity::class)]
    private $activities;

    #[ORM\Column(type: 'text')]
    private $baseline;

    #[ORM\Column(type: 'text')]
    private $excerpt;

    #[ORM\Column(type: 'text')]
    private $content2;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'content2Image')]
    private ?File $content2ImageFile = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $content2Image;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Parcours::class)]
    private $parcours;

    #[ORM\Column(type: 'string', length: 100, unique: true)]
    #[Gedmo\Slug(fields:['title'])]
    private $slug;

    #[ORM\ManyToOne(targetEntity: Status::class, inversedBy: 'articles')]
    private $status;


    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }



    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->name = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->parcours = new ArrayCollection();

    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection<int, Images>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setArticle($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getArticle() === $this) {
                $image->setArticle(null);
            }
        }

        return $this;
    }

    public function getCategories(): ?Categories
    {
        return $this->categories;
    }

    public function setCategories(?Categories $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return Collection<int, Comments>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getAuteur(): ?Users
    {
        return $this->auteur;
    }

    public function setAuteur(?Users $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getCountries(): ?Countries
    {
        return $this->countries;
    }

    public function setCountries(?Countries $countries): self
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->setArticle($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getArticle() === $this) {
                $activity->setArticle(null);
            }
        }

        return $this;
    }

    public function getBaseline(): ?string
    {
        return $this->baseline;
    }

    public function setBaseline(string $baseline): self
    {
        $this->baseline = $baseline;

        return $this;
    }

    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    public function setExcerpt(string $excerpt): self
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    public function getContent2(): ?string
    {
        return $this->content2;
    }

    public function setContent2(string $content2): self
    {
        $this->content2 = $content2;

        return $this;
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return File|null
     */
    public function getContent2ImageFile(): ?File
    {
        return $this->content2ImageFile;
    }

    /**
     * @param File|null $content2ImageFile
     */
    public function setContent2ImageFile(?File $content2ImageFile): void
    {
        $this->content2ImageFile = $content2ImageFile;
    }

    /**
     * @return File|null
     */
    public function getHeroImageFile(): ?File
    {
        return $this->HeroImageFile;
    }

    /**
     * @param File|null $HeroImageFile
     */
    public function setHeroImageFile(?File $HeroImageFile): void
    {
        $this->HeroImageFile = $HeroImageFile;

        if (null !== $HeroImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return File|null
     */
    public function getImageIntersectionFile(): ?File
    {
        return $this->imageIntersectionFile;
    }

    /**
     * @param File|null $imageIntersectionFile
     */
    public function setImageIntersectionFile(?File $imageIntersectionFile): void
    {
        if (null !== $imageIntersectionFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }


        $this->imageIntersectionFile = $imageIntersectionFile;
    }

    public function getHeroImage(): ?string
    {
        return $this->HeroImage;
    }




    public function setHeroImage(?string $HeroImage): self
    {
        $this->HeroImage = $HeroImage;

        return $this;
    }

    public function getContentImage(): ?string
    {
        return $this->contentImage;
    }

    public function setContentImage(string $contentImage): self
    {
        $this->contentImage = $contentImage;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getContentImageFile(): ?File
    {
        return $this->contentImageFile;
    }

    /**
     * @param File|null $contentImageFile
     */
    public function setContentImageFile(?File $contentImageFile): void
    {
        if (null !== $contentImageFile) {
        // It is required that at least one field changes if you are using doctrine
        // otherwise the event listeners won't be called and the file is lost
        $this->updateAt = new \DateTimeImmutable();
    }
        $this->contentImageFile = $contentImageFile;

    }

    public function getImageIntersection(): ?string
    {
        return $this->imageIntersection;
    }

    public function setImageIntersection(?string $imageIntersection): self
    {
        $this->imageIntersection = $imageIntersection;

        return $this;
    }

    public function getContent2Image(): ?string
    {
        return $this->content2Image;
    }

    public function setContent2Image(?string $content2Image): self
    {
        $this->content2Image = $content2Image;

        return $this;
    }

    /**
     * @return Collection<int, Parcours>
     */
    public function getParcours(): Collection
    {
        return $this->parcours;
    }

    public function addParcour(Parcours $parcour): self
    {
        if (!$this->parcours->contains($parcour)) {
            $this->parcours[] = $parcour;
            $parcour->setArticle($this);
        }

        return $this;
    }

    public function removeParcour(Parcours $parcour): self
    {
        if ($this->parcours->removeElement($parcour)) {
            // set the owning side to null (unless already changed)
            if ($parcour->getArticle() === $this) {
                $parcour->setArticle(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }



}
