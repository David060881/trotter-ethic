<?php

namespace App\Entity;

use App\Repository\CarnetVoyageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;


#[ORM\Entity(repositoryClass: CarnetVoyageRepository::class)]
#[Vich\Uploadable]

class CarnetVoyage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $Content;

    #[Vich\UploadableField(mapping: 'carnet_image', fileNameProperty: 'thumbnail')]
    private ?File $imageFile = null;

    #[ORM\Column(type: 'string', length: 255)]
    private  $thumbnail;


    #[Vich\UploadableField(mapping: 'carnet_image', fileNameProperty: 'imageContent')]
    private ?File $imageContentFile = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private  $imageContent;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $updateAt;

    #[ORM\ManyToOne(targetEntity: Users::class, inversedBy: 'carnetVoyages')]
    private $users;

    #[ORM\Column(type: 'string', length: 100, unique: true)]
    #[Gedmo\Slug(fields:['title'])]
    private $slug;

    #[ORM\ManyToOne(targetEntity: Countries::class, inversedBy: 'carnetVoyages')]
    #[ORM\JoinColumn(nullable: false)]
    private $country;

    #[ORM\ManyToOne(targetEntity: Parcours::class, inversedBy: 'carnetVoyages')]
    private $parcour;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }/**
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageContentFile(): ?File
    {
        return $this->imageContentFile;
    }
    /**
     * @param File|null $imageContentFile
     */
    public function setImageContentFile(?File $imageContentFile): void
    {
        if (null !== $imageContentFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
        $this->imageContentFile = $imageContentFile;
    }

    public function getImageContent(): ?string
    {
        return $this->imageContent;
    }

    public function setImageContent(?string $imageContent): self
    {
        $this->imageContent = $imageContent;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }/**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    public function getCountry(): ?Countries
    {
        return $this->country;
    }

    public function setCountry(?Countries $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getParcour(): ?Parcours
    {
        return $this->parcour;
    }

    public function setParcour(?Parcours $parcour): self
    {
        $this->parcour = $parcour;

        return $this;
    }
}
