<?php

namespace App\DataFixtures;

use App\Entity\Activity;
use App\Entity\Article;
use App\Entity\Categories;
use App\Entity\CategoriesInterest;
use App\Entity\Comments;
use App\Entity\Countries;
use App\Entity\GeneralSetting;
use App\Entity\Images;
use App\Entity\IntersetPoint;
use App\Entity\Status;
use App\Entity\Users;
use App\Entity\UsersProfile;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AppFixture extends Fixture
{
        function randomDate(){
            $d = random_int(1,28);
            $m = random_int(1,12);
            $y = random_int(2019,2022);
            $h = random_int(0,23);
            $i = random_int(0,59);
            return $y . '-' . $m . '-' . $d . '  ' . $h . ':' . $i;
        }

        private UserPasswordHasherInterface $encoder;

        public function __construct(UserPasswordHasherInterface $encoder){
            $this->encoder = $encoder;
        }

    public function load(ObjectManager $manager): void
    {
        // commentaire
        $strCom = array(
            "J'étais prêt à tourner la page, mais c'est la page qui ne veut pas se tourner.",
            "Écoute... Je n'aime pas faire la morale, mais je vais te donner un conseil qui te servira à jamais. Dans la vie tu rencontreras beaucoup de cons.",
            "Le coeur d'une femme est un océan de secrets.",
            "J'ai des mains faites pour l'or et elles sont dans la merde.",
            "Si je te dis de me parler d'art, tu vas me balancer un condensé de tous les livres sur le sujet. Michel-Ange, tu sais plein de trucs sur lui. Sur son oeuvre, sur ses choix politiques, sur lui et sur le pape, ses tendances sexuelles, tout le bazar quoi. Mais je parie que ce qu'on respire dans la Chapelle Sixtine, son odeur, tu connais pas. Tu ne peux pas savoir ce que c'est que de lever les yeux sur le magnifique plafond. Tu sais pas."
        );




        $stat1 = new Status();
        $stat1->setLabel("Brouillon");
        $stat1->setInternalName('draft');
        $manager->persist($stat1);

        $stat2 = new Status();
        $stat2->setLabel("Publié");
        $stat2->setInternalName('publish');
        $manager->persist($stat2);

        $stat3 = new Status();
        $stat3->setLabel("A supprimer");
        $stat3->setInternalName('to_delete');
        $manager->persist($stat3);

        $country = new Countries();
        $country->setName('France')
            ->setClimat('tempéré')
            ->setDescription("La France, pays de l'Europe occidentale, compte des villes médiévales, des villages alpins et des plages. Paris, sa capitale, est célèbre pour ses maisons de mode,")
            ->setLongitude(-0.36331670647421094)
            ->setLatitude(49.18800169975168);

        $country2 = new Countries();
        $country2->setName('Cameroun')
            ->setClimat('Chaud')
            ->setDescription("Le Cameroun, en forme longue la république du Cameroun, est un État d'Afrique centrale, situé entre le Nigeria au nord-nord-ouest,")
            ->setLatitude(3.852559081363188)
            ->setLongitude(11.51215060838948);

        $country3 = new Countries();
        $country3->setName('Argentine')
            ->setClimat('Chaud')
            ->setDescription("L'Argentine est un grand pays d'Amérique du Sud au relief très varié où se côtoient les montagnes des Andes, les lacs glaciaires et la pampa")
            ->setLatitude( -34.60401412706319)
            ->setLongitude(-58.42029419190588);

        $country4 = new Countries();
        $country4->setName('Myanmar')
            ->setClimat('Varié')
            ->setDescription("Myanmar (ex-Birmanie) est un pays de l'Asie du Sud-Est regroupant plus de 100 groupes ethniques et ayant une frontière commune avec l'Inde")
            ->setLatitude( 21.965182547585187)
            ->setLongitude(96.09092821428533);




        $manager->persist($country);
        $manager->persist($country2);
        $manager->persist($country3);
        $manager->persist($country4);
        $manager->flush();




        // creation administrateur

        $admin = new Users();

        $admin->setEmail('ducrocqdavid50@gmail.com')
            ->setFirstname('David')
            ->setLastname('Ducrocq')
            ->setPseudo('David')
            ->setPassword($this->encoder->hashPassword($admin, '123admin2020'))
            ->setRoles(["ROLE_ADMIN","ROLE_USER"])
            ->setCreatedAt(new \DateTime());

        $userProfile = new UsersProfile();
        $userProfile->setUser($admin)
            ->setBiography("Passionné depuis toujours par l’environnement informatique, j’ai choisi à 39 ans de me reconvertir. Pour ce faire, j’ai rejoint le groupe FIM en septembre 2020, afin de réaliser un BTS développeur intégrateur de medias interactifs. ")
            ->setCreatedAt(new \Datetime());
        $manager->persist($userProfile);
        $manager->persist($admin);
        $manager->flush();

        // creation users


        $user1 = new Users();
        $user1->setFirstname('Marcel')
            ->setLastname('Cerdan')
            ->setPseudo('VoyageurHeureux')
            ->setPassword($this->encoder->hashPassword($user1, 'EdithForever69'))
            ->setRoles(["ROLE_USER"])
            ->setEmail('Marcel@Cerdan.com')
            ->setCreatedAt(new \DateTime());

        $userProfile1 = new UsersProfile();
        $userProfile1->setUser($user1)
            ->setCreatedAt(new \Datetime());
        $manager->persist($userProfile1);
        $manager->persist($user1);
        $manager->flush();


        $user2 = new Users();
        $user2->setFirstname('Cassius')
            ->setLastname('Clay')
            ->setPseudo('Naturoman')
            ->setPassword($this->encoder->hashPassword($user1, 'StingLikeABee'))
            ->setRoles(["ROLE_USER"])
            ->setEmail('Cassius@Clay.com')
            ->setCreatedAt(new \DateTime());
        $userProfile2 = new UsersProfile();
        $userProfile2->setUser($user2)
            ->setCreatedAt(new \Datetime());
        $manager->persist($userProfile2);
        $manager->persist($user2);
        $manager->flush();


        $user3 = new Users();
        $user3->setFirstname('Rocky')
            ->setLastname('Balboa')
            ->setPseudo('RockMyWorld')
            ->setPassword($this->encoder->hashPassword($user1, 'Adrieeenne!'))
            ->setRoles(["ROLE_USER"])
            ->setEmail('Rocky@Balboa.com')
            ->setCreatedAt(new \DateTime());
        $userProfile3 = new UsersProfile();
        $userProfile3->setUser($user3)
            ->setCreatedAt(new \Datetime());
        $manager->persist($userProfile3);
        $manager->persist($user3);
        $manager->flush();


        $user4 = new Users();
        $user4->setFirstname('Apollo')
            ->setLastname('Creed')
            ->setPseudo('Appolon')
            ->setPassword($this->encoder->hashPassword($user1, 'RockyIsWeak'))
            ->setRoles(["ROLE_USER"])
            ->setEmail('Apollo@Creed.com')
            ->setCreatedAt(new \DateTime());
        $userProfile4 = new UsersProfile();
        $userProfile4->setUser($user4)
            ->setCreatedAt(new \Datetime());
        $manager->persist($userProfile4);
        $manager->persist($user4);
        $manager->flush();



        $UserTab = array(
            $user1,
            $user2,
            $user3,
            $user4,
            $admin
        );

        $countryTab = array(
            $country,
            $country2,
            $country3,
            $country4,
        );

        //creation articles
        for ($i = 0; $i < 5; $i++) {
            $article = new Article();
            $date = $this->randomDate();
            $article->setTitle("Titre article " . $i + 1)
                ->setBaseline("le meilleur article")
                ->setExcerpt('ceci est un extrait du meilleur article écrit')
                ->setContent("Contenu de l'article numéro " . $i + 1)
                ->setContent2("Contenu de l'article numéro " . $i + 1)
                ->setCreatedAt(new DateTime($date))
                ->setUpdateAt(new DateTime($date))
                ->setAuteur($UserTab[rand(0, count($UserTab) - 1)])
                ->setCountries($countryTab[rand(0, count($countryTab) - 1)])
                ->setContentImage('https://picsum.photos/800/600.webp')
                ->setThumbnail("https://picsum.photos/800/600.webp")
                ->setStatus($stat2);

            $activity = new Activity();
            $activity->setArticle($article)
                ->setName('activité'. $i +1)
                ->setDescription('Activité test')
                ->setLatitude(0)
                ->setLongitude(0);

            $manager->persist($article);
            $manager->persist($activity);

        }
        $manager->flush();



        //creation point d'interet 

        $categoryInterset1 = new  CategoriesInterest; 

        $categoryInterset1
            ->setTitle('Restaurant')
            ->setDescription('Filtre par restaurant');
            $manager->persist($categoryInterset1);
            $manager->flush();



        $pointInterest = new IntersetPoint();

        $pointInterest ->setTitle('Restaurant traditionnel')
            ->setDescription('le top du top')
            ->setLatitude(1.25200)
            ->setLongitude(2.2522)
            ->setCategorieInterest($categoryInterset1);
            $manager->persist($pointInterest);
            $manager->flush();

    }
}
